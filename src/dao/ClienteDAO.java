/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import dominio.Cidade;
import dominio.Cliente;
import java.sql.SQLException;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import java.util.List;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;

/**
 *
 * @author 1547816
 */
public class ClienteDAO extends GenericDAO {

    public ClienteDAO()  {
        dao.ConexaoHibernate.getSessionFactory();
    }

    
    
    private List<Cliente> pesquisar(int tipo, String pesq) {
        return listar(Cliente.class);
    }
    
    public List<Cliente> pesquisarNome(String pesq) {             
        return pesquisar(1,pesq);
    }
    
    public List<Cliente> pesquisarBairro(String pesq) {             
        return pesquisar(2,pesq);
    }
    
    public List contPorBairro() {
        List lista = null;
        Session sessao = null;
        try {
            sessao = ConexaoHibernate.getSessionFactory().openSession();
            sessao.beginTransaction();

            // CRITERIA
            Criteria cons = sessao.createCriteria(Cliente.class);

            // Montar o PROJECTION                               
            cons.setProjection( Projections.projectionList()
                    .add(Projections.count("idCliente") )
                    .add(Projections.groupProperty("bairro") )
            );
            
            lista = cons.list();

            sessao.getTransaction().commit();
            sessao.close();
            
         } catch ( HibernateException ex ) {
            if ( sessao != null) {
                sessao.getTransaction().rollback();
                sessao.close();
            }
            
            throw new HibernateException(ex);
        }
        return lista;
    }
}
