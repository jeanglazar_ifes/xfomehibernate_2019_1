/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;


import dominio.Pedido;
import java.sql.SQLException;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;


/**
 *
 * @author 1547816
 */
public class PedidoDAO extends GenericDAO {

    public PedidoDAO()  {
        dao.ConexaoHibernate.getSessionFactory();
    }

    
    
    private List<Pedido> pesquisar(int tipo, String pesq) {
        List lista = null;
        Session sessao = null;
        
        try {
            // Abrir a SESSÃO
            sessao = ConexaoHibernate.getSessionFactory().openSession();
            sessao.getTransaction().begin(); 
            
            Criteria consulta = sessao.createCriteria(Pedido.class);
            
            consulta.setFetchMode("itensPedido", FetchMode.JOIN);            
            consulta.setResultTransformer( Criteria.DISTINCT_ROOT_ENTITY );
            consulta.createAlias("cliente", "cli");
            //consulta.createAlias("itensPedido", "ip");
            //consulta.createAlias("itensPedido.chaveComposta.lanche", "lan", JoinType.NONE );

            
            switch (tipo) {
                case 0: consulta.add( Restrictions.eq( "idPedido", Integer.parseInt(pesq))  );                
                        break;                
                case 1: consulta.add( Restrictions.like( "cli.nome", pesq+"%")  );                
                        break;
                case 2: 
                        //consulta.add( Restrictions.like( "lan.nome", pesq+"%")  );
                        consulta.createCriteria("itensPedido")
                                .createCriteria("chaveComposta.lanche")
                                .setFetchMode("chaveComposta.lanche", FetchMode.SELECT)
                                //.createAlias("lanche", "lan")
                                .add( Restrictions.like( "nome", pesq+"%")  );
                        break;                                                
                case 3: consulta.add( Restrictions.like( "cli.bairro", pesq+"%")  );
                        break;                
                case 4: 
                        String vetor[] = pesq.split("/");
                        consulta.add( Restrictions.sqlRestriction("MONTH(data) = " + vetor[0] 
                                + " and YEAR(data) = " + vetor[1]) );
                        break;                
            }
            
            lista = consulta.list();

            sessao.getTransaction().commit(); 
            sessao.close();
        } catch ( HibernateException ex ) {
            if ( sessao != null) {
                sessao.getTransaction().rollback();
                sessao.close();
            }
            
            throw new HibernateException(ex);
        }
        return lista;  
    }
    
    public List<Pedido> pesquisarPorID(String pesq) {
         return pesquisar(0,pesq);             
    }
    
    public List<Pedido> pesquisarPorCliente(String pesq) {             
        return pesquisar(1,pesq);
    }

    public List<Pedido> pesquisarPorLanche(String pesq) {             
         return pesquisar(2,pesq);
    }
    
    public List<Pedido> pesquisarPorBairro(String pesq) {             
        return pesquisar(3,pesq);
    }
        
    public List<Pedido> pesquisarPorMes(String pesq) {             
        return pesquisar(4,pesq);
    }
    
    public List valorPorMes() throws SQLException, Exception {
        List lista = null;
        Session sessao = null;
        try {
            sessao = ConexaoHibernate.getSessionFactory().openSession();
            sessao.beginTransaction();

            // CRITERIA
            Criteria cons = sessao.createCriteria(Pedido.class);

            // Definir o PROJECTION
            cons.setProjection( Projections.projectionList()
                    .add( Projections.sum("valorTotal"))
                    .add(Projections.sqlGroupProjection(
                        "YEAR(data) as ano, MONTH(data) as mes",
                        "ano, mes",
                        new String[] {"ano", "mes"},
                        new Type[] { StandardBasicTypes.STRING, StandardBasicTypes.STRING  }
                    ))
            );

            lista = cons.list();

            sessao.getTransaction().commit();
            sessao.close();
        } catch ( HibernateException ex ) {
            if ( sessao != null) {
                sessao.getTransaction().rollback();
                sessao.close();
            }
            
            throw new HibernateException(ex);
        }
        return lista; 

    }
    
    
    public List valorPorCliente() throws SQLException, Exception {
        List lista = null;
        Session sessao = null;
        try {
            sessao = ConexaoHibernate.getSessionFactory().openSession();
            sessao.beginTransaction();

            // CRITERIA
            Criteria cons = sessao.createCriteria(Pedido.class);

            // Definir o PROJECTION
            cons.setProjection( Projections.projectionList()
                    .add( Projections.sum("valorTotal"))
                    .add( Projections.groupProperty("cliente"))
            );

            lista = cons.list();

            sessao.getTransaction().commit();
            sessao.close();
        } catch ( HibernateException ex ) {
            if ( sessao != null) {
                sessao.getTransaction().rollback();
                sessao.close();
            }
            
            throw new HibernateException(ex);
        }
        return lista; 

    }
}
