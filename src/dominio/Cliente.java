/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import gerenciador.FuncoesUteis;
import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.*;
import javax.swing.ImageIcon;

/**
 *
 * @author 1547816
 */        

@Entity
public class Cliente implements Serializable {
    
    @Id
    @GeneratedValue ( strategy = GenerationType.IDENTITY )
    private int idCliente;
       
    @Column ( nullable = false)
    private String nome;
    
    @Column ( length = 14, unique = true)
    private String cpf;
    
    @Column ( updatable = false)
    @Temporal ( TemporalType.DATE )
    private Date dtNasc; 
    
    private char sexo;
    private String ender;
    private int num;
    private String complemento;
    private String bairro;
    private String referencia;
    private String telFixo;
    private String celular;    
    private String email;
    
    @Lob
    private byte[] foto;
        
    @ManyToOne ( fetch = FetchType.EAGER )
    @JoinColumn ( name="idCidade" )
    private Cidade cidade;
    
    @OneToMany (mappedBy = "cliente", fetch = FetchType.LAZY)
    private List<Pedido> pedidos = new ArrayList();
    
    // Construtor VAZIO para o HIBERNATE
    public Cliente() {
    }
    
    
    public Cliente(int idCliente) {
        this.idCliente = idCliente;
    }

    
    
    public Cliente(String nome, String cpf, Date dtNasc, char sexo, String ender, int num, String complemento, String bairro, String referencia, String telFixo, String celular, String email, byte[] foto, Cidade cidade) {
        this.nome = nome;
        this.cpf = cpf;
        this.dtNasc = dtNasc;
        this.sexo = sexo;
        this.ender = ender;
        this.num = num;
        this.complemento = complemento;
        this.bairro = bairro;
        this.referencia = referencia;
        this.telFixo = telFixo;
        this.celular = celular;
        this.email = email;
        this.cidade = cidade;
        this.foto = foto;
    }

    
    public Cliente(int idCliente, String nome, String cpf, Date dtNasc, char sexo, String ender, int num, String complemento, String bairro, String referencia, String telFixo, String celular, String email, byte[] foto, Cidade cidade) {
        this.idCliente = idCliente;
        this.nome = nome;
        this.cpf = cpf;
        this.dtNasc = dtNasc;
        this.sexo = sexo;
        this.ender = ender;
        this.num = num;
        this.complemento = complemento;
        this.bairro = bairro;
        this.referencia = referencia;
        this.telFixo = telFixo;
        this.celular = celular;
        this.email = email;
        this.cidade = cidade;
        this.foto = foto;
    }

    public List<Pedido> getPedidos() {
        return pedidos;
    }

    public void setPedidos(List<Pedido> pedidos) {
        this.pedidos = pedidos;
    }

           
    public byte[] getFoto() {
        return foto;
    }

    public ImageIcon getFotoImage() {
        if ( foto != null )
            return new ImageIcon( foto );
        else
            return null;
    }
    
    public void setFoto(byte[] foto) {
        this.foto = foto;
    }

    public Cidade getCidade() {
        return cidade;
    }

    public void setCidade(Cidade cidade) {
        this.cidade = cidade;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Date getDtNasc() {
        return dtNasc;
    }

    public String getDtNascFormatada() {
        try {
            return FuncoesUteis.dateToStr(dtNasc);
        } catch (ParseException ex) {
            return "";
        }
    }
    
    public void setDtNasc(Date dtNasc) {
        this.dtNasc = dtNasc;
    }

    public char getSexo() {
        return sexo;
    }

    public void setSexo(char sexo) {
        this.sexo = sexo;
    }

    public String getEnder() {
        return ender;
    }

    public void setEnder(String ender) {
        this.ender = ender;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public String getTelFixo() {
        return telFixo;
    }

    public void setTelFixo(String telFixo) {
        this.telFixo = telFixo;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }
    
    public Object[] toArray() {
        return new Object[] {this, bairro, cidade, getDtNascFormatada(), celular, getFotoImage() };
    }

    
    @Override
    public String toString() {
        return nome;
    }

    
}
