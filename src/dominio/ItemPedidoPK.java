/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import java.io.Serializable;
import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author 1547816
 */

@Embeddable
public class ItemPedidoPK implements Serializable {
    
    @ManyToOne
    @JoinColumn ( name = "idLanche" )
    private Lanche lanche;
    
    @ManyToOne
    @JoinColumn ( name = "idPedido" )
    private Pedido pedido;

    public ItemPedidoPK() {
    }

    public ItemPedidoPK(Pedido pedido, Lanche lanche) {
        this.lanche = lanche;
        this.pedido = pedido;
    }

    public Pedido getPedido() {
        return pedido;
    }

    public void setPedido(Pedido pedido) {
        this.pedido = pedido;
    }

    public Lanche getLanche() {
        return lanche;
    }

    public void setLanche(Lanche lanche) {
        this.lanche = lanche;
    }
    
    
    
}
