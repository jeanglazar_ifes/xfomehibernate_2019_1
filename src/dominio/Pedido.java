/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import gerenciador.FuncoesUteis;
import java.io.Serializable;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.*;
import org.hibernate.annotations.Cascade;


/**
 *
 * @author 1547816
 */

@Entity
public class Pedido implements Serializable {
   
    @Id
    @GeneratedValue ( strategy = GenerationType.IDENTITY )
    private int idPedido;
    
    @Temporal ( TemporalType.DATE )
    private Date data;     
    
    private String observacao;
//    private int maisBife;
//    private int maisBacon;
//    private int maisPresunto;
//    private int maisQueijo;

    private char entregar;    
    private float valorTotal;

    @ManyToOne ( fetch = FetchType.EAGER )
    @JoinColumn ( name="idCliente" )
    private Cliente cliente;
    
    @OneToMany ( mappedBy = "chaveComposta.pedido", fetch = FetchType.LAZY )  
    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE )
    private List<ItemPedido> itensPedido = new ArrayList();

    // Construtor VAZIO para o HIBERNATE
    public Pedido() {
    }

    public Pedido(Date data, String observacao, char entregar, float valorTotal, Cliente cliente) {
        this.data = data;
        this.observacao = observacao;
        this.entregar = entregar;
        this.valorTotal = valorTotal;
        this.cliente = cliente;
    }

    public Pedido(int idPedido, Date data, String observacao, char entregar, float valorTotal, Cliente cliente) {
        this.idPedido = idPedido;
        this.data = data;
        this.observacao = observacao;
        this.entregar = entregar;
        this.valorTotal = valorTotal;
        this.cliente = cliente;
    }

    public int getIdPedido() {
        return idPedido;
    }

    public void setIdPedido(int idPedido) {
        this.idPedido = idPedido;
    }

    public Date getData() {
        return data;
    }

    public String getDataStr() {
        try {
            return FuncoesUteis.dateToStr(data);
        } catch (ParseException ex) {
            return "";
        }
    }
    
    public void setData(Date data) {
        this.data = data;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public char getEntregar() {
        return entregar;
    }

    public void setEntregar(char entregar) {
        this.entregar = entregar;
    }

    public float getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(float valorTotal) {
        this.valorTotal = valorTotal;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public List<ItemPedido> getItensPedido() {
        return itensPedido;
    }

    public void setItensPedido(List<ItemPedido> itensPedido) {
        this.itensPedido = itensPedido;
    }


    @Override
    public String toString() {
        return String.valueOf( idPedido );
    }
  
    
    public Object[] toArray() {   
        
        // FORMATAR MOEDA
        NumberFormat formNum = NumberFormat.getCurrencyInstance();
        
        /*
        // FORMATAR CASAS DECIMAIS 
        DecimalFormat formNum = new DecimalFormat();
        formNum.setMaximumFractionDigits(2);
        formNum.setMinimumFractionDigits(2);
        */
        
        return new Object[] {this, cliente, cliente.getBairro(), 
                             getDataStr(), 
                             formNum.format(valorTotal)  };        
    }

}
