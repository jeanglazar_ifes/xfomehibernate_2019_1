/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gerenciador;

import dao.*;
import dominio.Cidade;
import dominio.Cliente;
import dominio.ItemPedido;
import dominio.Lanche;
import dominio.Pedido;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Date;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.util.List;
import javax.imageio.ImageIO;
import javax.imageio.stream.ImageOutputStream;
import javax.swing.Icon;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import org.hibernate.HibernateException;

/**
 *
 * @author 1547816
 */
public class GerenciadorDominio {

    GenericDAO genDao;
    ClienteDAO cliDao;
    PedidoDAO pedDao;
    

    public GerenciadorDominio() throws ClassNotFoundException, SQLException {
        genDao = new GenericDAO();
        cliDao = new ClienteDAO();        
        pedDao = new PedidoDAO();

    }

    // FUNÇÃO GENÉRICA
    public void excluirObj(Object obj) throws SQLException {
        genDao.excluir(obj);
    }
    
    // FUNÇÃO GENÉRICA
    public List listar(Class classe) throws HibernateException {
        return genDao.listar(classe);
    }

    public int inserirCliente(String nome, String cpf, Date dtNasc, char sexo,
            String ender, int num, String complemento, String bairro,
            String referencia, String telFixo, String celular, String email, Icon foto, Cidade cidade) throws SQLException, ClassNotFoundException {

        Cliente cli = new Cliente(nome, cpf, dtNasc, sexo, ender,
                num, complemento, bairro, referencia, telFixo, celular, email, FuncoesUteis.iconToBytes(foto), cidade);

        // ClienteDAO
        cliDao.inserir(cli);
        return cli.getIdCliente();
    }

    public void alterarCliente(int idCliente, String nome, String cpf, Date dtNasc, char sexo,
            String ender, int num, String complemento, String bairro,
            String referencia, String telFixo, String celular, String email, Icon foto, Cidade cidade) throws SQLException, ClassNotFoundException {

        Cliente cli = new Cliente(nome, cpf, dtNasc, sexo, ender,
                num, complemento, bairro, referencia, telFixo, celular, email, FuncoesUteis.iconToBytes(foto), cidade);

        cli.setIdCliente(idCliente);

        // ClienteDAO
        cliDao.alterar(cli);
    }

    
    public List pesquisarCliente(int tipo, String pesq, JTable tabela) {
        List<Cliente> lista = null;

        switch (tipo) {
            case 0:
                lista = cliDao.pesquisarNome(pesq);
                break;
                //    case 1: lista = cliDao.pesquisarID(pesq); break;
            case 2:
                lista = cliDao.pesquisarBairro(pesq);
                break;
        }
        for ( Cliente cli : lista) {
            ((DefaultTableModel) tabela.getModel()).addRow( cli.toArray() );
            
            cli.setPedidos(  pedDao.pesquisarPorCliente(cli.getNome())   );
        }
        return lista;
    }

    public int inserirPedido(Cliente cliente, char entregar, JTable tabLanches) {
         
        float valorTotal = 0;
        Pedido ped = new Pedido(new Date(), "", entregar, valorTotal, cliente);

        // Percorrer a TABELA de Lanches e insere em PEDIDO na lista de ITEMPEDIDO
        List<ItemPedido> listaItens = ped.getItensPedido();
        
        int tam = tabLanches.getRowCount();
        if (tam > 0) {
            for (int i = 0; i < tam; i++) {                
                int col=1;
                Lanche lan = (Lanche) tabLanches.getValueAt(i, col++);
                int maisBife = Integer.parseInt( tabLanches.getValueAt(i, col++).toString() );
                int maisQueijo = Integer.parseInt( tabLanches.getValueAt(i, col++).toString() );
                int maisPresunto = Integer.parseInt( tabLanches.getValueAt(i, col++).toString() );
                int maisOvo = Integer.parseInt( tabLanches.getValueAt(i, col++).toString() );
                String ingredientes = tabLanches.getValueAt(i, col++).toString();
                
                ItemPedido item = new ItemPedido(ped, lan, ingredientes, maisBife, maisOvo, maisPresunto, maisQueijo );
                listaItens.add(item);
                valorTotal = valorTotal + lan.getValor();
            }

            ped.setValorTotal(valorTotal);

            genDao.inserir(ped);
            return ped.getIdPedido();
        } else {
            return -1;
        }

    }
    
    public void pesquisarPedido(int tipo, String pesq, JTable tabela) {
        List<Pedido> lista = null;

        ((DefaultTableModel) tabela.getModel()).setRowCount(0);
        switch (tipo) {
            case 0: 
                lista = pedDao.pesquisarPorID(pesq);
                break;
            case 1:
                lista = pedDao.pesquisarPorCliente(pesq);
                break;                
            case 2:
                lista = pedDao.pesquisarPorLanche(pesq);
                break;
            case 3:
                lista = pedDao.pesquisarPorBairro(pesq);
                break;
            case 4:
                lista = pedDao.pesquisarPorMes(pesq);
                break;

        }
        
        if ( lista != null && lista.size() > 0 ) {
            for ( Pedido ped : lista) {
                ((DefaultTableModel) tabela.getModel()).addRow( ped.toArray() );
            }
        } else {
            JOptionPane.showMessageDialog(null, "Não existem pedidos.");
        }

    }

    public void relGroupBy(JTable tabela, int tipo) throws Exception {
        List<Object[]> lista = null;
        Cliente cli = null;

        // Limpa a tabela
        ((DefaultTableModel) tabela.getModel()).setRowCount(0);

        switch (tipo) {
            case 'B':
                lista = cliDao.contPorBairro();
                break;
            case 'M':
                lista = pedDao.valorPorMes();
                break;
            case 'C':
                lista = pedDao.valorPorCliente();
                break;
        }

        NumberFormat formato = NumberFormat.getCurrencyInstance();
        // Percorrer a LISTA
        if (lista != null) {

            String col0 = "";
            String col1 = "";
            for (Object[] obj : lista) {
                switch (tipo) {
                    case 'B':
                        col0 = obj[0].toString();
                        col1 = obj[1].toString();
                        break;                        
                    case 'M':
                        col0 = formato.format( Double.parseDouble( obj[0].toString() ));
                        col1 = obj[2].toString() + "/" + obj[1].toString();                        
                        break;
                    case 'C':
                        col0 = formato.format( Double.parseDouble( obj[0].toString() ) );
                        col1 = obj[1].toString();
                        break;
                        
                }

                Object[] novo = new Object[]{col1, col0};
                ((DefaultTableModel) tabela.getModel()).addRow(novo);
            }

        }
    }

    private byte[] IconToBytes(Icon icon) {
        if ( icon == null ) {
            return null;
        }
        BufferedImage img = new BufferedImage(icon.getIconWidth(), icon.getIconHeight(), BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2d = img.createGraphics();
        icon.paintIcon(null, g2d, 0, 0);
        g2d.dispose();

        byte[] bFile = null;
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            ImageOutputStream ios = ImageIO.createImageOutputStream(baos);
            try {
                ImageIO.write(img, "png", ios);
                // Set a flag to indicate that the write was successful
            } finally {
                ios.close();
            }
            bFile = baos.toByteArray();
        } catch (IOException ex) {
            bFile = null;
        }
        finally {
            return bFile;
        } 

    } 
}
