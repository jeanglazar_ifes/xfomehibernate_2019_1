/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gerenciador;

import dominio.Cliente;
import dominio.Pedido;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import org.hibernate.HibernateException;
import visao.DlgCadCliente;
import visao.DlgCadPedido;
import visao.DlgPesqCliente;
import visao.DlgPesqPedido;
import visao.FrmPrincipal;
import visao.RelGroupBy;

/**
 *
 * @author 1547816
 */
public class GerenciadorIHC {
    
    private GerenciadorDominio gerDominio;
    private GerenciadorRelatorios gerRel;
    private FrmPrincipal janPrinc = null;
    
    
    public GerenciadorIHC() {
        try {
            gerDominio = new GerenciadorDominio();
            gerRel = new GerenciadorRelatorios();
        } catch (ClassNotFoundException ex) {
            JOptionPane.showMessageDialog(null, "Classe de BD não encontrada.", "Erro conexão", JOptionPane.ERROR_MESSAGE);
            System.exit(-1);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage() , "Erro conexão", JOptionPane.ERROR_MESSAGE);
            System.exit(-1);            
        }
    }

    public GerenciadorDominio getGerDominio() {
        return gerDominio;
    }
    
    public GerenciadorRelatorios getGerRel() {
        return gerRel;
    }
    
    public void janelaPrincipal() {
        janPrinc = new FrmPrincipal(this);
        janPrinc.setVisible(true);

    }
    
    public void janelaCadCliente() {
        DlgCadCliente janCliente = new DlgCadCliente(janPrinc, true, this);
        janCliente.setVisible(true);

    }
    
    public Cliente janelaPesqCliente() {
        DlgPesqCliente janPesqCliente = new DlgPesqCliente(janPrinc, true, this);
        janPesqCliente.setVisible(true);
        return janPesqCliente.getCliSelecionado();

    }
    
    public void janelaCadPedido() {
        DlgCadPedido janPedido = new DlgCadPedido(janPrinc, true, this);
        janPedido.setVisible(true);

    }
    
    public Pedido janelaPesqPedido() {
        DlgPesqPedido janPesqPedido = new DlgPesqPedido(janPrinc, true, this);
        janPesqPedido.setVisible(true);
        return janPesqPedido.getPedidoSelecionado();

    }
    
    public void janelaRelGroupBy() {
        RelGroupBy janRel = new RelGroupBy(janPrinc, true, this);
        janRel.setVisible(true);

    }
    
    public void preencherCombo(JComboBox combo, Class classe) {
        List lista;
        try {
            lista = gerDominio.listar(classe);
            combo.setModel(  new DefaultComboBoxModel(lista.toArray() )  );
                                    
        } catch (HibernateException ex) {
            Logger.getLogger(GerenciadorIHC.class.getName()).log(Level.SEVERE, null, ex);
        }   
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                // Nimbus
                // Windows
                // Windows Classic
                // CDE/Motif
                // Metal
                // System.out.println( info.getName()  );
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrmPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrmPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrmPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        GerenciadorIHC gerIHC = new GerenciadorIHC();
        gerIHC.janelaPrincipal();
                
        
        /* Create and display the form */

/*        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FrmPrincipal().setVisible(true);
            }
        });
*/
    }
}
